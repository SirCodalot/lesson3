#include "Vector.h"

// CONSTRUCTORS AND DESTRUCTORS

Vector::Vector(int n)
{
	if (n < 2)
		n = 2;

	_elements = new int[n];
	_size = 0;
	_capacity = n;
	_resizeFactor = n;
}

Vector::Vector(const Vector& other)
{
	_size = other.size();
	_capacity = other.capacity();
	_resizeFactor = other.resizeFactor();
	_elements = new int[_capacity];

	for (int i = 0; i < _size; i++)
		_elements[i] = other[i];
}

Vector::~Vector()
{
	delete[] _elements;
}


// OPERATOR OVERRIDERS

Vector& Vector::operator=(const Vector& other)
{
	return *(new Vector(other));
}

int& Vector::operator[](int n) const
{
	if (n >= _size)
	{
		std::cout << "error: index is greater than the vector's size" << std::endl;
		return _elements[0];
	}

	return _elements[n];
}

void Vector::operator+=(const Vector& other)
{
	for (int i = 0; i < _size; i++)
		if (i < other.size())
			_elements[i] += other[i];
}

void Vector::operator-=(const Vector& other)
{
	for (int i = 0; i < _size; i++)
		if (i < other.size())
			_elements[i] -= other[i];
}


Vector operator+(const Vector& a, const Vector& b)
{
	Vector* vector = new Vector(0);
	int size = a.size() < b.size() ? b.size() : a.size();

	for (int i = 0; i < size; i++)
	{
		int value = a.size() < i ? a[i] : 0;
		if (b.size() < i)
			value += b[i];
		vector->push_back(value);
	}

	return *vector;
}

Vector operator-(const Vector& a, const Vector& b)
{
	Vector* vector = new Vector(0);
	int size = a.size() < b.size() ? b.size() : a.size();

	for (int i = 0; i < size; i++)
	{
		int value = a.size() < i ? a[i] : 0;
		if (b.size() < i)
			value -= b[i];
		vector->push_back(value);
	}

	return *vector;
}

std::ostream& operator<<(std::ostream& os, const Vector& vector)
{
	os << "Vector Info:\n";
	os << "Capability is " << vector.capacity() << "\n";
	os << "Size is " << vector.size() << "\n";
	os << "Data is {";
	for (int i = 0; i < vector.size(); i++)
		os << vector[i] << ((vector.size() - 1 == i) ? "" : ", ");
	os << "}" << std::endl;

	return os;
}


// GETTERS AND SETTERS

int Vector::size() const
{
	return _size;
}

int Vector::capacity() const
{
	return _capacity;
}

int Vector::resizeFactor() const
{
	return _resizeFactor;
}

void Vector::resize(int n)
{
	resize(n, NULL);
}

void Vector::resize(int n, const int& val)
{
	if (n <= _capacity)
	{
		int from = _size < n ? _size : n;
		int to = _size < n ? n : _size;

		for (int i = from; i < to; i++)
			_elements[i] = NULL;

		_size = n;
	}
	else
	{
		reserve(n, val);
		_size = n;
	}
}


// OTHER FUNCTIONS

bool Vector::empty() const
{
	return _size == 0;
}

void Vector::push_back(const int& val)
{
	if (_size >= _capacity)
		reserve(_size + 1);

	_elements[_size] = val;
	_size++;
}


int Vector::pop_back()
{
	if (_size <= 0)
	{
		std::cout << "error: pop from empty vector";
		return -9999;
	}
	int val = _elements[_size - 1];
	resize(_size - 1);
	return val;
}

void Vector::reserve(int n)
{
	reserve(n, NULL);
}

void Vector::reserve(int n, const int& val)
{
	while (_capacity < n)
		_capacity += _resizeFactor;

	int* elements = new int[_capacity];
	for (int i = 0; i < n; i++)
	{ 
		if (i < _size)
			elements[i] = _elements[i];
		else
			elements[i] = val;
	}
	delete[] _elements;
	_elements = elements;

}

void Vector::assign(int val)
{
	_size = _capacity;

	for (int i = 0; i < _size; i++)
		_elements[i] = val;
}
